//package com.lzugis.netcdf.util;
//
//import org.apache.commons.lang.StringUtils;
//import org.osgeo.proj4j.*;
//import uk.ac.rdg.resc.edal.util.GridCoordinates2D;
//
///**
// * Created by lixiaohai on 2017/5/18.
// */
//public class GridIndexLoader {
//    public static final String WGS84_PARAM = "+title=long/lat:WGS84 +proj=longlat +datum=WGS84 +units=degrees";
//
//    public static final CoordinateTransformFactory CT_FACTORY = new CoordinateTransformFactory();
//    public static final CRSFactory CRS_FACTORY = new CRSFactory();
//
//    /**
//     * 通过经纬度获取下标。通过投影信息。四至信息
//     */
//    public static GridCoordinates2D proj4j(double lon, double lat, String tgtCRS, String ncExtent) {
//        if (StringUtils.isNotEmpty(tgtCRS)) {
//            CoordinateTransform trans = CT_FACTORY.createTransform(createCRS(WGS84_PARAM), createCRS(tgtCRS));
//            ProjCoordinate pout = new ProjCoordinate();
//            ProjCoordinate p = new ProjCoordinate(lon, lat);
//            trans.transform(p, pout);
//            lon = pout.x;
//            lat = pout.y;
//        }
//        return proj4j(lon, lat, ncExtent);
//    }
//
//    public static GridCoordinates2D proj4j(double lon, double lat, CoordinateTransform trans, String ncExtent) {
//        if (trans != null) {
//            ProjCoordinate pout = new ProjCoordinate();
//            ProjCoordinate p = new ProjCoordinate(lon, lat);
//            trans.transform(p, pout);
//            lon = pout.x;
//            lat = pout.y;
//        }
//        return proj4j(lon, lat, ncExtent);
//    }
//
//    /**
//     * 通过经纬度获取下标。通过四至信息
//     */
//    public static GridCoordinates2D proj4j(double lon, double lat, String ncExtent) {
//        String[] extent = ncExtent.split(";");
//        double minX = Double.parseDouble(extent[0].split(",")[0]);
//        double minY = Double.parseDouble(extent[1].split(",")[0]);
//        double spacingX = Double.parseDouble(extent[0].split(",")[2]);
//        double spacingY = Double.parseDouble(extent[1].split(",")[2]);
//        int xIndex = (int) ((lon - minX) / spacingX + 0.5);
//        int yIndex = (int) ((lat - minY) / spacingY + 0.5);
//        return new GridCoordinates2D(xIndex, yIndex);
//    }
//
//    public static CoordinateReferenceSystem createCRS(String crsSpec) {
//        CoordinateReferenceSystem crs = null;
//        if (crsSpec.indexOf("+") >= 0 || crsSpec.indexOf("=") >= 0) {
//            crs = CRS_FACTORY.createFromParameters("Anon", crsSpec);
//        } else {
//            crs = CRS_FACTORY.createFromName(crsSpec);
//        }
//        return crs;
//    }
//
//    public static double[] index2LatLon(GridCoordinates2D index, String tgtCRS, String ncExtent) {
//        double[] latLon = index2LatLon(index, ncExtent);
//        if (StringUtils.isNotBlank(tgtCRS)) {
//            CoordinateTransform trans = CT_FACTORY.createTransform(createCRS(tgtCRS), createCRS(WGS84_PARAM));
//            ProjCoordinate pout = new ProjCoordinate();
//            ProjCoordinate p = new ProjCoordinate(latLon[1], latLon[0]);
//            trans.transform(p, pout);
//            // lat
//            latLon[0] = pout.y;
//            // lon
//            latLon[1] = pout.x;
//        }
//        return latLon;
//    }
//
//    public static double[] index2LatLon(GridCoordinates2D index, String ncExtent) {
//        String[] extent = ncExtent.split(";");
//        double minX = Double.parseDouble(extent[0].split(",")[0]);
//        double minY = Double.parseDouble(extent[1].split(",")[0]);
//        double spacingX = Double.parseDouble(extent[0].split(",")[2]);
//        double spacingY = Double.parseDouble(extent[1].split(",")[2]);
//        double lon = index.getX() * spacingX + minX;
//        double lat = index.getY() * spacingY + minY;
//        return new double[]{lat, lon};
//    }
//
//    public static  String buildSectionSpec(int varDims, int x, int y, int startTime, int endTime) {
//        if (varDims == 2) {
//            return y + ":" + y + ":1," + x + ":" + x + ":1";
//        } else if (varDims == 3) {
//            return startTime + ":" + endTime + ":1," + y + ":" + y + ":1," + x + ":" + x + ":1";
//        }
//        return "";
//    }
//
//
//}
