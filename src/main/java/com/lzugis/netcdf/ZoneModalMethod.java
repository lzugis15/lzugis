//package com.lzugis.netcdf;
//
//import com.lzugis.CommonMethod;
//import com.lzugis.netcdf.util.GridIndexLoader;
//import com.vividsolutions.jts.geom.*;
//import org.geotools.data.shapefile.ShapefileDataStore;
//import org.geotools.data.simple.SimpleFeatureCollection;
//import org.geotools.data.simple.SimpleFeatureIterator;
//import org.geotools.data.simple.SimpleFeatureSource;
//import org.json.simple.JSONObject;
//import org.opengis.feature.simple.SimpleFeature;
//import uk.ac.rdg.resc.edal.util.GridCoordinates2D;
//
//import java.io.File;
//import java.io.IOException;
//import java.nio.charset.Charset;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * @author nunu
// * @date 2017年11月16日 下午1:59:58
// * @Description 经纬度取下标
// */
//public class ZoneModalMethod {
//    private static List provinces;
//    private static CommonMethod cm = new CommonMethod();
//
//    private String shpfile = "D:\\nfdw\\shenshan\\shenshan.shp";
//
//    public ZoneModalMethod() {
//        try {
//            System.out.println("开始初始化边界。");
//            initBoundryShp();
//            System.out.println("完成初始化边界。");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    /**
//     * 初始化边界shp
//     */
//    private void initBoundryShp() {
//        try {
//            //初始化shape
//            File file = new File(shpfile);
//            ShapefileDataStore shpDataStore = null;
//            shpDataStore = new ShapefileDataStore(file.toURI().toURL());
//            //设置编码
//            Charset charset = Charset.forName("GBK");
//            shpDataStore.setCharset(charset);
//            String typeName = shpDataStore.getTypeNames()[0];
//            SimpleFeatureSource featureSource = shpDataStore.getFeatureSource(typeName);
//            SimpleFeatureCollection result = featureSource.getFeatures();
//            SimpleFeatureIterator itertor = result.features();
//
//            provinces = new ArrayList();
//
//            while (itertor.hasNext()) {
//                SimpleFeature feature = itertor.next();
//                String name = (String) feature.getAttribute("name");
//                Geometry geom = (Geometry) feature.getAttribute("the_geom");
//                Map map = new HashMap();
//                Envelope envelope = geom.getEnvelopeInternal();
//                map.put("name", name);
//                map.put("geom", geom);
//                map.put("min", new double[]{envelope.getMinX(), envelope.getMinY()});
//                map.put("max", new double[]{envelope.getMaxX(), envelope.getMaxY()});
//                provinces.add(map);
//            }
//            itertor.close();
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    /**
//     * 获取区域蒙板xian
//     *
//     * @param zoneData
//     * @return
//     */
//    public Map getZoneModalJson(Map zoneData) {
//        Map map = new HashMap();
//        double[] min = (double[]) zoneData.get("min"),
//                max = (double[]) zoneData.get("max");
//        MultiPolygon geom = (MultiPolygon) zoneData.get("geom");
//        String name = zoneData.get("name").toString();
//        map.put("cname", name);
//        System.out.println("正在计算" + name + "......");
//
//        int[] indexMin = getIndexByLatlon((float) min[1], (float) min[0]),
//                indexMax = getIndexByLatlon((float) max[1], (float) max[0]);
//        int xmin = indexMin[1],
//                ymin = indexMin[0],
//                xmax = indexMax[1],
//                ymax = indexMax[0];
//        map.put("xmin", xmin - 1);
//        map.put("ymin", ymin - 1);
//        map.put("xmax", xmax + 1);
//        map.put("ymax", ymax + 1);
//        List list = new ArrayList();
//        for (int i = xmin - 1; i <= xmax + 1; i++) {
//            List _list = new ArrayList();
//            for (int j = ymin - 1; j <= ymax + 1; j++) {
//                float[] latlon = getLatlonByIndex(i, j);
//                Geometry gLonlat = new GeometryFactory().createPoint(new Coordinate(latlon[1], latlon[0]));
//                int isIn = gLonlat.within(geom) ? 1 : -1;
//                _list.add(isIn);
//            }
//            list.add(_list);
//        }
//        map.put("data", list);
//        return map;
//    }
//
//    int[] getIndexByLatlon(float lat, float lon) {
//        GridCoordinates2D gridCoordinates2D = GridIndexLoader.proj4j(lon, lat,
//                "+proj=merc +lon_0=110 +lat_ts=30 +x_0=0 +y_0=0 +a=6370000 +units=m +no_defs",
//                "-1661424.2059025052,1661424.2059025052,3468.526525892495;1722330.847949752,3750465.2081908197,3449.2080956480745");
//        return new int[]{gridCoordinates2D.getY(), gridCoordinates2D.getX()};
//    }
//
//    float[] getLatlonByIndex(int x, int y) {
//        GridCoordinates2D gridCoordinates2D = new GridCoordinates2D(x, y);
//        double[] doubles = GridIndexLoader.index2LatLon(gridCoordinates2D, "+proj=merc +lon_0=110 +lat_ts=30 +x_0=0 +y_0=0 +a=6370000 +units=m +no_defs",
//                "-1661424.2059025052,1661424.2059025052,3468.526525892495;1722330.847949752,3750465.2081908197,3449.2080956480745");
//        return new float[]{(float) doubles[0], (float) doubles[1]};
//    }
//
//    /**
//     * 测试类
//     *
//     * @param args
//     * @throws IOException
//     */
//    public static void main(String[] args) throws IOException {
//        ZoneModalMethod china = new ZoneModalMethod();
//        long start = System.currentTimeMillis();
////        Map province = (Map) provinces.get(4);
////        Map map = china.getZoneModalJson(province);
////        String txtPath = "C:\\Users\\lzuni\\Desktop\\test\\" + province.get("name") + ".json";
////        cm.append2File(txtPath, JSONObject.toJSONString(map));
////        System.out.println("\r\n共耗时" + (System.currentTimeMillis() - start) + "ms");
//        for (int i = 0; i < provinces.size(); i++) {
//            Map province = (Map) provinces.get(i);
//            Map map = china.getZoneModalJson(province);
//            String txtPath = "C:\\Users\\lzuni\\Desktop\\test\\" + province.get("name") + ".json";
//            cm.append2File(txtPath, JSONObject.toJSONString(map));
//        }
//        System.out.println("\r\n共耗时" + (System.currentTimeMillis() - start) + "ms");
//    }
//}
